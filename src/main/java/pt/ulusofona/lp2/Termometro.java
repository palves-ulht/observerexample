package pt.ulusofona.lp2;

/**
 * Created by pedroalves on 11/01/17.
 */
public class Termometro {

    private int temperaturaAtual = 0;

    public void aquece() {
        temperaturaAtual++;
    }

    public void arrefece() {
        temperaturaAtual--;
    }

    public int getTemperaturaAtual() {
        return temperaturaAtual;
    }
}
