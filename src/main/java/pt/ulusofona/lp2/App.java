package pt.ulusofona.lp2;

import java.util.Scanner;

/**
 * Created by pedroalves on 11/01/17.
 */
public class App {

    public static void main(String[] args) {

        AlertaVisual alertaVisual = new AlertaVisual();
        AlertaSonoro alertaSonoro = new AlertaSonoro();
        Termometro termometro = new Termometro();

        System.out.println("Introduza '+' para aquecer ou '-' para arrefecer");

        Scanner teclado = new Scanner(System.in);

        while (true) {

            String palavra = teclado.next();

            if (palavra.equals("+")) {
                termometro.aquece();
            }

            if (palavra.equals("-")) {
                termometro.arrefece();
            }

        }


    }
}
