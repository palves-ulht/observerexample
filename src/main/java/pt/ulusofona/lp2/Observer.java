package pt.ulusofona.lp2;

/**
 * Created by pedroalves on 11/01/17.
 */
public interface Observer {

    public void update(Observable observable, String propertyName, Object newValue);
}


