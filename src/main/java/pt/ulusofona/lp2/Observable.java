package pt.ulusofona.lp2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pedroalves on 11/01/17.
 */
public class Observable {

    private List<Observer> observers = new ArrayList<Observer>();

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public void remoteObserver(Observer observer) {
        observers.remove(observer);
    }

    public void setChanged(String propertyName, Object newValue) {
        for (Observer observer : observers) {
            observer.update(this, propertyName, newValue);
        }
    }
}
